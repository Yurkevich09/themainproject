

package Controller;

import com.antion.figuresfx.figures.Circle;
import com.antion.figuresfx.figures.Figure;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import org.apache.log4j.Logger;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

public class MainScreenViewController implements Initializable {
    private Figure[] figures;
    private Random random;

    public static void main(String[] args) {
        try{
            if( figure is null) throw new Exception("The figure is null");


        }
        catch(Exception ex){

            System.out.println(ex.getMessage());

        }
    }

    @FXML
    private Canvas canvas;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        figures = new Figure[1];
        random = new Random(System.currentTimeMillis());
        System.out.println("Successfully initialized!");
    }

    private void addFigure(Figure figure) {
        if (figures[figures.length - 1] == null) {
            figures[figures.length - 1] = figure;
            return;
        }
        Figure[] tmp = new Figure[figures.length + 1];
        int index = 0;

        for (; index < figures.length; index++) {
            tmp[index] = figures[index];
        }
        tmp[index] = figure;
        figures = tmp;
    }


    private Figure createFigure(double x, double y) {
        Figure figure = null;

        switch (random.nextInt(3)) {
            case Figure.FIGURE_TYPE_CIRCLE:
                figure = new Circle(Figure.FIGURE_TYPE_CIRCLE, x, y, random.nextInt(3), Color.RED, random.nextInt(50));
            case Figure.FIGURE_TYPE_RECTANGLE:
                break;
            case Figure.FIGURE_TYPE_TRIANGLE:
                break;
            default:
                System.out.println("Unknown figure!");
        }
        return figure;
    }

    private void repaint() {
        canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        for(Figure figure: figures) {
            if(figure != null) {
                figure.draw(canvas.getGraphicsContext2D());
            }
        }
    }


        @FXML
    public void onMouseClicked(MouseEvent event) {
        addFigure(createFigure(event.getX(), event.getY()));
        repaint();
    }
}
public class Log4j11 {

    private static Logger log = Logger.getLogger(SomeClass.class.getName());

    public void someMethod()
    {
        log.info("Some message");
    }


