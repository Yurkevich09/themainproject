package com.antion.figuresfx.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.Objects;

public class Triangle extends Figure {
  private double halfBase;

  public Triangle(double cx, double cy, double lineWidth, Color color) {
    super(FIGURE_TYPE_TRIANGLE, cx, cy, lineWidth, color);
  }

  public Triangle(double cx, double cy, double lineWidth, Color color, double halfBase) {
    this(cx, cy, lineWidth, color);
    this.halfBase = halfBase < 10 ? 10 : halfBase;
  }

  public double getHalfBase() {
    return halfBase;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Triangle triangle = (Triangle) o;
    return Double.compare(triangle.halfBase, halfBase) == 0;
  }

  @Override
  public int hashCode() {
    return Objects.hash(halfBase);
  }

  @Override
  public String toString() {
    return "com.antion.figuresfx.figures.Triangle{" +
            ", halfBase=" + halfBase +
            '}';
  }

  @Override
  public void draw(GraphicsContext gc) {
    gc.setLineWidth(lineWidth);
    gc.setStroke(color);
    gc.strokePolygon(new double[]{cx, cx - halfBase, cx + halfBase},
            new double[]{cy - halfBase, cy + halfBase, cy + halfBase}, 3);
  }
}
